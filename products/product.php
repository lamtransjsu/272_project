<?php include '../connect/connect.php';
error_reporting(0);
@ini_set('display_errors', 0);
?>
<?php
include '../connect/Cookie_Utils.php';
$companyId = (int)$_GET['companyId'];
$product_id = (int)$_GET['productId'];
setMarketCookie($companyId, $product_id, $cookie_recent_visited, $cookie_most_visited);
if ($companyId == 2) {
    setMarketCookie($companyId, $product_id, $cookie_recent_visited_2, $cookie_most_visited_2);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-U   A-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Product Page</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="./styles.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <script src="script/product_userRating.js" type="text/javascript"></script>

</head>
<body>
<!-- Top most nav bar -->
<nav class="navbar navbar-default navbar-static-top">
    <!--    <div class="container-fluid">-->
    <ul class="menu">
        <li><a class="iconic home" href="../index.php">The Awesome Company</a></li>
        <li>
            <?php
            $companyPage = "../company/company.php";
            ?>
            <a>Company A<span class="iconic"></span></a>
            <ul>
                <?php
                $companyInfo = $companyPage . "?companyId=1&";
                ?>
                <li><a href="<?php echo $companyInfo . 'sorting=1' ?>">Best Seller</a></li>
                <li><a href="<?php echo $companyInfo . 'sorting=2' ?>">Highest Rated</a></li>
                <li><a href="<?php echo $companyInfo . 'sorting=3' ?>">Most visited</a></li>
                <li><a href="<?php echo $companyInfo . 'sorting=4' ?>">Recent visited</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="<?php echo $companyInfo . 'sorting=5' ?>">All Products</a></li>
            </ul>
        </li>
        <li>
            <a>Company B<span class="iconic"></span></a>
            <ul>
                <?php
                $companyInfo = $companyPage . "?companyId=2&";
                ?>
                <li><a href="<?php echo $companyInfo . 'sorting=1' ?>">Best Seller</a></li>
                <li><a href="<?php echo $companyInfo . 'sorting=2' ?>">Highest Rated</a></li>
                <li><a href="<?php echo $companyInfo . 'sorting=3' ?>">Most visited</a></li>
                <li><a href="<?php echo $companyInfo . 'sorting=4' ?>">Recent visited</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="<?php echo $companyInfo . 'sorting=5' ?>">All Products</a></li>
            </ul>
        </li>
        <li>
            <a>Company C<span class="iconic"></span></a>
            <ul>
                <?php
                $companyInfo = $companyPage . "?companyId=3&";
                ?>
                <li><a href="<?php echo $companyInfo . 'sorting=1' ?>">Best Seller</a></li>
                <li><a href="<?php echo $companyInfo . 'sorting=2' ?>">Highest Rated</a></li>
                <li><a href="<?php echo $companyInfo . 'sorting=3' ?>">Most visited</a></li>
                <li><a href="<?php echo $companyInfo . 'sorting=4' ?>">Recent visited</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="<?php echo $companyInfo . 'sorting=5' ?>">All Products</a></li>
            </ul>
        </li>
        <li>
            <a>Company D<span class="iconic"></span></a>
            <ul>
                <?php
                $companyInfo = $companyPage . "?companyId=4&";
                ?>
                <li><a href="<?php echo $companyInfo . 'sorting=1' ?>">Best Seller</a></li>
                <li><a href="<?php echo $companyInfo . 'sorting=2' ?>">Highest Rated</a></li>
                <li><a href="<?php echo $companyInfo . 'sorting=3' ?>">Most visited</a></li>
                <li><a href="<?php echo $companyInfo . 'sorting=4' ?>">Recent visited</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="<?php echo $companyInfo . 'sorting=5' ?>">All Products</a></li>
            </ul>
        </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <!-- facebook login -->
        <?php
        session_start();
        if (empty($_SESSION['FBID'])) {
            echo '<a href="../lib/facebook/fbconfig.php"><img class="fb-img" src="../facebookLogin.jpg"  /></a>';
        } else {
            echo '<a href="../lib/facebook/logout.php"><img class="fb-img" src="../facebookLogout.jpg"  /></a>';
        }
        ?>
        <a href="../checkout/checkout.php">
            <img class="nav-cart" src="http://mir-lamp.com.ua/img/cart.png" alt="cart"/>
        </a>
    </ul>
    <!--    </div>-->
</nav>

<!--creating different sections using bootstrap grid system-->
<div class="container-fluid main-content">
    <div class="row">
        <!--        <div class="col-xs-2"><h1>cookies should go here, there is no code yet!</h1></div>-->
        <div class="col-sm-8 col-sm-offset-1">
            <div class="row">
                <!--                <div class="col-sm-9">-->
                <!--                    <div class="productImg">-->
                <!--read the product description and its image url from SESSION and then write it on top of product review form-->
                <?php

                //receive productId and companyId from product_session.php
                $productId = $_GET['productId'];
                $companyId = $_GET['companyId'];

                //get all the product list as a 2D array from SESSION
                $prod_list = $_SESSION['prod_list'];

                //loop through this array and find the respective productId and companyId
                for ($i = 0; $i < count($prod_list); $i++) {
                    if ($prod_list[$i][0] == $companyId && $prod_list[$i][1] == $productId) {
//                                print("<p><img src = " . $prod_list[$i][4] . " width='250' height='250' style=\"float: left; text-align: center; margin: 0.5em; padding: 0.5em;\" />" . $prod_list[$i][3] . "</p>");
                        echo("
                       <div class=\"img-container\">
					   <div class=\"center-block\"> <img src=\" " . $prod_list[$i][4] . " \" alt=\"Thumbnail Image 1\" class=\"img-responsive center-block\" width=\"400\" height=\"400\">
					   <div class=\"caption\">
						<h1 class=\"ellipsis\">" . $prod_list[$i][2] . "</h1>
						<p class=\"ellipsis\">" . $prod_list[$i][3] . "</p>
						<p class=\"ellipsis\">$" . $prod_list[$i][5] . "</p>
						    </div>
                                </div>
                                </div>");

                    }
                }


                ?>


                <div class="col-xs-12"><br>
                    <button class="btn btn-primary btn-sm" type="submit" value="Add" id="addCart">
                        <span class="glyphicon glyphicon-shopping-cart"
                              id="shoppingCart"></span><span>Add to cart</span>
                        <!--sending all the information of the product as an array to product_addCart.js-->
                        <?PHP
                        //receive productId and companyId from product_session.php
                        $productId = $_GET['productId'];
                        $companyId = $_GET['companyId'];

                        //get all the product list as a 2D array from SESSION
                        $prod_list = $_SESSION['prod_list'];

                        //loop through this array and find the respective productId and companyId
                        for ($i = 0; $i < count($prod_list); $i++) {
                            if ($prod_list[$i][0] == $companyId && $prod_list[$i][1] == $productId) {
                                $company_id = $prod_list[$i][0];
                                $prod_id = $prod_list[$i][1];
                                $prod_name = $prod_list[$i][2];
                                $prod_description = $prod_list[$i][3];
                                $prod_image_url = $prod_list[$i][4];
                                $prod_price = $prod_list[$i][5];
                            }
                        }
                        //test
                        /*$company_id = 1;
                        $prod_id = 2;
                        $prod_name = 3;
                        $prod_description = 4;
                        $prod_image_url = 5;
                        $prod_price = 6;*/
                        ?>
                    </button>
                    <script type="text/javascript">
                        var company_id = "<?PHP echo $company_id; ?>";
                        var prod_id = "<?PHP echo $prod_id; ?>";
                        var prod_name = "<?PHP echo $prod_name; ?>";
                        var prod_description = "<?PHP echo $prod_description; ?>";
                        var prod_image_url = "<?PHP echo $prod_image_url; ?>";
                        var prod_price = "<?PHP echo $prod_price; ?>";
                    </script>
                    <script src="script/product_addCart.js" type="text/javascript"></script>
                </div>
                <div class="col-sm-12"><h1>How Many Stars You Give Me?</h1>
                    <div>
                        <span class="glyphicon glyphicon-star star-color" id="r6"></span>
                        <span class="glyphicon glyphicon-star star-color" id="r7"></span>
                        <span class="glyphicon glyphicon-star star-color" id="r8"></span>
                        <span class="glyphicon glyphicon-star star-color" id="r9"></span>
                        <span class="glyphicon glyphicon-star star-color" id="r10"></span>
                    </div>
                    <div><h4 class="rateSlideDown"></h4></div>
                    <div>
                        <div class="form-group">
                            <textarea class="form-control" id="review" name="review" cols="40" rows="5" required
                                      placeholder="Write your review up to 500 characters"></textarea>
                        </div>
                        <input class="btn btn-primary btn-lg" id="btnRate"
                               data-companyId="<?php echo $companyId; ?>" data-productId="<?php echo $productId; ?>"
                               type="submit"
                               value="Rate">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-12"></div>
                        <div class="col-xs-12">
                            <h1 class="name">
                                <!--read the product name and its price from SESSION and then write it on top of add to cart button-->
                                <?PHP
                                // $con = mysqli_connect("handsomemengzeng.com", "cmpe272", "123456", "cmpe272project");

                                //receive productId and companyId from product_session.php
                                $productId = $_GET['productId'];
                                $companyId = $_GET['companyId'];

                                //get all the product list as a 2D array from SESSION
                                $prod_list = $_SESSION['prod_list'];

                                //loop through this array and find the respective productId and companyId
                                //                                for ($i = 0; $i < count($prod_list); $i++) {
                                //                                    if ($prod_list[$i][0] == $companyId && $prod_list[$i][1] == $productId) {
                                //                                        print($prod_list[$i][2]);
                                //                                        print("<br>");
                                //                                        print($prod_list[$i][5] . " USD");
                                //                                    }
                                //                                }

                                //close the database
                                // mysqli_close($con);
                                ?>
                            </h1>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <!--                <div class="col-sm-7">-->
            <!--                    <form method="post" action="product_review.php">-->
            <!--                        <div class="form-group">-->
            <!--                            <textarea class="form-control" name="review" cols="40" rows="5" required-->
            <!--                                      placeholder="Write your review up to 500 characters"></textarea>-->
            <!--                        </div>-->
            <!--                        <input class="btn btn-primary btn-md" type="submit" value="Submit Review">-->
            <!--                    </form>-->
            <!--                </div>-->

<!--           <div class="col-xs-2">
                <label for="comment">Reviews:</label>
                <!--            <textarea class="form-control" rows="30" id="comment">-->
                <?PHP
                $con = mysqli_connect("handsomemengzeng.com", "cmpe272", "123456", "cmpe272project");

                //receive productId and companyId from product_session.php
                $productId = $_GET['productId'];
                $companyId = $_GET['companyId'];

                //check if its connected
                if (!$con) {
                    die("connection to the server error!");
                } else {
                    //query all reviews about this product
                    $product_sql = "SELECT rating, review, u.userName FROM ProductReview p, USER u WHERE productId=$productId AND companyId=$companyId AND u.userId=p.userId";
//                print($product_sql);
                    $product_result = mysqli_query($con, $product_sql);
                    for ($counter = 0; $row = mysqli_fetch_row($product_result); $counter++) {
//                    foreach ($row as $key => $value) {
//                    print_r($row);
                        ?>
                        <h2>
                            <?php
                            print("</br>");
                            print($row[2]);
                            print("</br>");
                            print('Rate: ' . $row[0]);
                            print("</br>");
                            print($row[1]);
                            ?>
                        </h2>

                        <?php
//                    print("</br>");
                    }
                }
                //close the database
                mysqli_close($con);
                ?>
                </textarea>
            </div>
        </div>

        </div>

</div>
<!-- Footer section -->
<footer class="footer">
    <div class="container-fluid col-xs-12 col-md-12">
        <div class="row">
            <?php
            $query4 = "SELECT * FROM Company Order by conpanyId ASC";
            $result = mysqli_query($conn, $query4);
            // default companyInfo
            $companyInfo = [['company1', 'http://ekladata.com/hK-Z6Etp4SF1Fo8JUL2_BeCAkj0@640x486.jpg'],
                ['company2', 'http://ekladata.com/hK-Z6Etp4SF1Fo8JUL2_BeCAkj0@640x486.jpg'],
                ['company3', 'http://ekladata.com/hK-Z6Etp4SF1Fo8JUL2_BeCAkj0@640x486.jpg'],
                ['company4', 'http://ekladata.com/hK-Z6Etp4SF1Fo8JUL2_BeCAkj0@640x486.jpg']];
            for ($counter = 0;
                 $row = mysqli_fetch_assoc($result);
                 $counter++) {
                $companyInfo[$counter] = [$row[companhName], $row[companyLink]];
            }
            for ($s = 0;
            $s < 4;
            $s++)
            {
            echo "<div class = 'col-xs-6 col-md-3'>";
            ?>
            <h5><?php echo $companyInfo[$s][0] ?></h5>
            <ul>
                <li><a href='<?php echo $companyInfo[$s][1] ?>'>Link to <?php echo $companyInfo[$s][0] ?></a></li>
            </ul>
        </div>
        <?php
        }
        ?>
    </div>
    <div class="copyright">
        <h5>Copyright @ 2016 Mengzeng Rao, Adel Sadrolgharavi, Bing Shi, Lam Tran</h5>
    </div>
</footer>

</body>
</html>
