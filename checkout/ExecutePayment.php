<?php
include '../connect/connect.php';
$success = $_GET['success'];
$paymentId = $_GET['paymentId'];
$token = $_GET['token'];
$PayerID = $_GET['PayerID'];

$sub_total = 0;
$products = array();

?>
<html>
<head>
    <meta charset="UTF-8">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="checkout.css" rel="stylesheet">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>The Awesome Company</title>
</head>

<body>
<!-- Top most nav bar -->
<nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
        <ul class="menu">
            <li><a class="iconic home" href="../index.php">The Awesome Company</a></li>
            <li>
                <?php
                $companyPage = "../company/company.php";
                ?>
                <a>Company A<span class="iconic"></span></a>
                <ul>
                    <?php
                    $companyInfo = $companyPage . "?companyId=1&";
                    ?>
                    <li><a href="<?php echo $companyInfo . 'sorting=1' ?>">Best Seller</a></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=2' ?>">Highest Rated</a></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=3' ?>">Most visited</a></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=4' ?>">Recent visited</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=5' ?>">All Products</a></li>
                </ul>
            </li>
            <li>
                <a>Company B<span class="iconic"></span></a>
                <ul>
                    <?php
                    $companyInfo = $companyPage . "?companyId=2&";
                    ?>
                    <li><a href="<?php echo $companyInfo . 'sorting=1' ?>">Best Seller</a></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=2' ?>">Highest Rated</a></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=3' ?>">Most visited</a></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=4' ?>">Recent visited</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=5' ?>">All Products</a></li>
                </ul>
            </li>
            <li>
                <a>Company C<span class="iconic"></span></a>
                <ul>
                    <?php
                    $companyInfo = $companyPage . "?companyId=3&";
                    ?>
                    <li><a href="<?php echo $companyInfo . 'sorting=1' ?>">Best Seller</a></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=2' ?>">Highest Rated</a></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=3' ?>">Most visited</a></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=4' ?>">Recent visited</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=5' ?>">All Products</a></li>
                </ul>
            </li>
            <li>
                <a>Company D<span class="iconic"></span></a>
                <ul>
                    <?php
                    $companyInfo = $companyPage . "?companyId=4&";
                    ?>
                    <li><a href="<?php echo $companyInfo . 'sorting=1' ?>">Best Seller</a></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=2' ?>">Highest Rated</a></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=3' ?>">Most visited</a></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=4' ?>">Recent visited</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo $companyInfo . 'sorting=5' ?>">All Products</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <!-- facebook login -->
            <?php
            session_start();
            if (empty($_SESSION['FBID'])) {
                echo '<a href="../lib/facebook/fbconfig.php"><img class="fb-img" src="../facebookLogin.jpg"  /></a>';
            } else {
                echo '<a href="../lib/facebook/logout.php"><img class="fb-img" src="../facebookLogout.jpg"  /></a>';
            }
            ?>
            <a href="checkout.php">
                <img class="nav-cart" src="http://mir-lamp.com.ua/img/cart.png" alt="cart"/>
            </a>
        </ul>
    </div>
</nav>
<?php
if ($success == 'true') {
    if (!isset($_SESSION['products_to_checkout'])) {
        header('Location: ../index.php');
    }

    ?>
    <div class="container">
        <div class="row text-size">
            <div class="col-xs-12">
                <div class="invoice-title">
                    <h2>Invoice</h2>
                </div>
                <hr>
                <div class="row">
                    <div class="col-xs-6 ">
                        <strong>Payment Method:</strong><br>
                        Paypal<br>
                        <?php
                        session_start();
                        $fbEmail = $_SESSION['EMAIL'];
                        echo "$fbEmail";
                        ?>
                    </div>
                    <div class="col-xs-6 text-right">
                        <strong>Order Date:</strong><br>
                        <?php
                        date_default_timezone_set('America/Los_Angeles');
                        echo(date("m d Y")) ?><br><br>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Order summary</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <td><strong>Item</strong></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right"><strong>Totals</strong></td>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                foreach ($_SESSION['products_to_checkout'] as $index => $item) {
                                    ?>

                                    <tr>
                                        <td><?php echo($item['name']) ?></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right">$<?php echo($item['price']) ?></td>
                                    </tr>

                                    <?php
                                    $sub_total += $item[5];
                                    $prod_item = array('companyId' => $item[companyId], 'id' => $item[id], 'name' => $item[name], 'des' => $item[des], 'price' => $item[price]);
                                    array_push($products, $prod_item);
                                }

                                $tax = $sub_total * 7.5 / 100;
                                $total = $sub_total + $tax;

                                $EMAIL = $_SESSION['EMAIL'];
                                send_success_email($EMAIL, $products, $_SESSION['products_to_checkout_total'], $_SESSION['products_to_checkout_tax']);

                                ?>
                                <tr>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line text-center">
                                        <strong>Subtotal</strong>
                                    </td>
                                    <td class="thick-line text-right">
                                        $<?php echo($_SESSION['products_to_checkout_subtotal']) ?></td>
                                </tr>
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-center"><strong>Tax</strong></td>
                                    <td class="no-line text-right">
                                        $<?php echo($_SESSION['products_to_checkout_tax']) ?></td>
                                </tr>
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-center"><strong>Total</strong></td>
                                    <td class="no-line text-right">
                                        $<?php echo($_SESSION['products_to_checkout_total']) ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="text-size" href="../index.php">Return to home</a>
    </div>

    <?php
    unset($_SESSION['products_to_checkout']);
    unset($_SESSION['products_to_checkout_tax']);
    unset($_SESSION['products_to_checkout_subtotal']);
    unset($_SESSION['products_to_checkout_total']);

} else {
    header('Location: checkout.php');
}


function send_success_email($email, $products, $total_price, $taxes)
{
    print($email);

    //$content .= "\n \n ------------------------------------------------------------------
    //            \n \t TOTAL:\t$" . $total_price;
    $content = '';
    foreach ($products as $index => $item) {
//        print_r($item);
        $content .= $item[name];
        $content .= '-----------------';
        $content .= '$' . $item[price];
        $content .= "\n";
    }

    $content .= "TAX: ";
    $content .= '$' . $taxes;

    $content .= "\nTOTAL: ";
    $content .= '$' . $total_price;
    $api_key = "api:key-286b464cd3a9183f0d03d3d0e9942093";
    $ch = curl_init('https://api.mailgun.net/v3/sandbox8b1c1d080b5a4a88ad11bb5d4ceefbe6.mailgun.org/messages');
    curl_setopt($ch, CURLOPT_USERPWD, $api_key);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array(
        'from' => 'Awesome Market Place <853394218@qq.com>',
        'to' => '<' . $email . '>',
        'subject' => 'Awesome Market Place Confirmation',
        'text' => "You already purchase: \n" . $content
    ));

    curl_exec($ch);
    curl_close($ch);
}

?>

<!-- Footer section -->
<footer class="footer">
    <div class="container-fluid col-xs-12 col-md-12">
        <div class="row">
            <?php
            $query4 = "SELECT * FROM Company Order by conpanyId ASC";
            $result = mysqli_query($conn, $query4);
            // default companyInfo
            $companyInfo = [['company1', 'http://ekladata.com/hK-Z6Etp4SF1Fo8JUL2_BeCAkj0@640x486.jpg'],
                ['company2', 'http://ekladata.com/hK-Z6Etp4SF1Fo8JUL2_BeCAkj0@640x486.jpg'],
                ['company3', 'http://ekladata.com/hK-Z6Etp4SF1Fo8JUL2_BeCAkj0@640x486.jpg'],
                ['company4', 'http://ekladata.com/hK-Z6Etp4SF1Fo8JUL2_BeCAkj0@640x486.jpg']];
            for ($counter = 0;
                 $row = mysqli_fetch_assoc($result);
                 $counter++) {
                $companyInfo[$counter] = [$row[companhName], $row[companyLink]];
            }
            for ($s = 0;
            $s < 4;
            $s++)
            {
            echo "<div class = 'col-xs-6 col-md-3'>";
            ?>
            <h5><?php echo $companyInfo[$s][0] ?></h5>
            <ul>
                <li><a href='<?php echo $companyInfo[$s][1] ?>'>Link to <?php echo $companyInfo[$s][0] ?></a></li>
            </ul>
        </div>
        <?php
        }
        ?>
    </div>
    <div class="copyright">
        <h5>Copyright @ 2016 Mengzeng Rao, Adel Sadrolgharavi, Bing Shi, Lam Tran</h5>
    </div>
</footer>
</body>
</html>

